\echo Exercice 3 : Requêtage

\echo Q1
SELECT n56, n57 + n58 + n59 AS ResultByMe FROM import limit 10;

\echo Q2
SELECT n56, n57 + n58 + n59 AS ResultByMe FROM import WHERE n57 + n58 + n59 <> n56;

\echo Q3
SELECT n74, ROUND((n51/(case WHEN n47 <> 0 THEN n47 ELSE 1 END))*100) AS ResultByMe FROM import limit 10;

\echo Q4
SELECT n74, ROUND((n51/(case WHEN n47 <> 0 THEN n47 ELSE 1 END))*100) AS ResultByMe FROM import GROUP BY n74, n47, n51 HAVING ROUND((n51/(case WHEN n47 <> 0 THEN n47 ELSE 1 END))*100)<> n74;

\echo Q5
SELECT n76, ROUND(n53/(case WHEN n47 <> 0 THEN n47 ELSE 1 END)*100) AS ResultsByMe FROM import LIMIT 10;

\echo Q7
SELECT n81, ROUND(n55/(case WHEN n56 <> 0 THEN n56 ELSE 1 END)*100) AS ResultByMe FROM import GROUP BY n81, n55, n56 LIMIT 10;