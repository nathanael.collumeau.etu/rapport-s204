--\! curl "https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B" > fr-esr-parcoursup.csv;

DROP TABLE IF EXISTS import CASCADE;

DROP TABLE IF EXISTS communes CASCADE;
DROP TABLE IF EXISTS etablissements CASCADE;
DROP TABLE IF EXISTS formations CASCADE;
DROP TABLE IF EXISTS candidats CASCADE;
DROP TABLE IF EXISTS admis CASCADE;

CREATE TABLE import (
    n1 INTEGER DEFAULT NULL,
    n2 CHAR(33) DEFAULT NULL,
    n3 CHAR(8) DEFAULT NULL,
    n4 TEXT DEFAULT NULL,
    n5 CHAR(3) DEFAULT NULL,
    n6 CHAR(24) DEFAULT NULL,
    n7 TEXT DEFAULT NULL,
    n8 TEXT DEFAULT NULL,
    n9 TEXT DEFAULT NULL,
    n10 TEXT DEFAULT NULL,
    n11 TEXT DEFAULT NULL,
    n12 TEXT DEFAULT NULL,
    n13 TEXT DEFAULT NULL,
    n14 TEXT DEFAULT NULL,
    n15 TEXT DEFAULT NULL,
    n16 TEXT DEFAULT NULL,
    n17 TEXT DEFAULT NULL,
    n18 TEXT DEFAULT NULL,
    n19 INTEGER DEFAULT NULL,
    n20 INTEGER DEFAULT NULL,
    n21 INTEGER DEFAULT NULL,
    n22 INTEGER DEFAULT NULL,
    n23 INTEGER DEFAULT NULL,
    n24 INTEGER DEFAULT NULL,
    n25 INTEGER DEFAULT NULL,
    n26 INTEGER DEFAULT NULL,
    n27 INTEGER DEFAULT NULL,
    n28 INTEGER DEFAULT NULL,
    n29 INTEGER DEFAULT NULL,
    n30 INTEGER DEFAULT NULL,
    n31 INTEGER DEFAULT NULL,
    n32 INTEGER DEFAULT NULL,
    n33 INTEGER DEFAULT NULL,
    n34 INTEGER DEFAULT NULL,
    n35 INTEGER DEFAULT NULL,
    n36 INTEGER DEFAULT NULL,
    n37 INTEGER DEFAULT NULL,
    n38 INTEGER DEFAULT NULL,
    n39 INTEGER DEFAULT NULL,
    n40 INTEGER DEFAULT NULL,
    n41 INTEGER DEFAULT NULL,
    n42 INTEGER DEFAULT NULL,
    n43 INTEGER DEFAULT NULL,
    n44 INTEGER DEFAULT NULL,
    n45 INTEGER DEFAULT NULL,
    n46 INTEGER DEFAULT NULL,
    n47 INTEGER DEFAULT NULL,
    n48 INTEGER DEFAULT NULL,
    n49 INTEGER DEFAULT NULL,
    n50 INTEGER DEFAULT NULL,
    n51 FLOAT DEFAULT NULL,
    n52 FLOAT DEFAULT NULL,
    n53 FLOAT DEFAULT NULL,
    n54 FLOAT DEFAULT NULL,
    n55 FLOAT DEFAULT NULL,
    n56 FLOAT DEFAULT NULL,
    n57 FLOAT DEFAULT NULL,
    n58 FLOAT DEFAULT NULL,
    n59 FLOAT DEFAULT NULL,
    n60 INTEGER DEFAULT NULL,
    n61 INTEGER DEFAULT NULL,
    n62 INTEGER DEFAULT NULL,
    n63 INTEGER DEFAULT NULL,
    n64 INTEGER DEFAULT NULL,
    n65 INTEGER DEFAULT NULL,
    n66 FLOAT DEFAULT NULL,
    n67 INTEGER DEFAULT NULL,
    n68 FLOAT DEFAULT NULL,
    n69 INTEGER DEFAULT NULL,
    n70 INTEGER DEFAULT NULL,
    n71 INTEGER DEFAULT NULL,
    n72 INTEGER DEFAULT NULL,
    n73 INTEGER DEFAULT NULL,
    n74 FLOAT DEFAULT NULL,
    n75 FLOAT DEFAULT NULL,
    n76 FLOAT DEFAULT NULL,
    n77 FLOAT DEFAULT NULL,
    n78 FLOAT DEFAULT NULL,
    n79 FLOAT DEFAULT NULL,
    n80 FLOAT DEFAULT NULL,
    n81 FLOAT DEFAULT NULL,
    n82 FLOAT DEFAULT NULL,
    n83 FLOAT DEFAULT NULL,
    n84 FLOAT DEFAULT NULL,
    n85 FLOAT DEFAULT NULL,
    n86 FLOAT DEFAULT NULL,
    n87 FLOAT DEFAULT NULL,
    n88 FLOAT DEFAULT NULL,
    n89 FLOAT DEFAULT NULL,
    n90 FLOAT DEFAULT NULL,
    n91 FLOAT DEFAULT NULL,
    n92 FLOAT DEFAULT NULL,
    n93 FLOAT DEFAULT NULL,
    n94 FLOAT DEFAULT NULL,
    n95 FLOAT DEFAULT NULL,
    n96 FLOAT DEFAULT NULL,
    n97 FLOAT DEFAULT NULL,
    n98 FLOAT DEFAULT NULL,
    n99 FLOAT DEFAULT NULL,
    n100 FLOAT DEFAULT NULL,
    n101 FLOAT DEFAULT NULL,
    n102 CHAR(39) DEFAULT NULL,
    n103 FLOAT DEFAULT NULL,
    n104 CHAR(39) DEFAULT NULL,
    n105 INTEGER DEFAULT NULL,
    n106 CHAR(39) DEFAULT NULL,
    n107 INTEGER DEFAULT NULL,
    n108 CHAR(45) DEFAULT NULL,
    n109 CHAR(19) DEFAULT NULL,
    n110 INTEGER DEFAULT NULL,
    n111 TEXT DEFAULT NULL,
    n112 TEXT DEFAULT NULL,
    n113 FLOAT DEFAULT NULL,
    n114 FLOAT DEFAULT NULL,
    n115 FLOAT DEFAULT NULL,
    n116 FLOAT DEFAULT NULL,
    n117 CHAR(5) DEFAULT NULL,
    n118 CHAR(5) DEFAULT NULL
);


\copy import FROM './fr-esr-parcoursup.csv' WITH (FORMAT CSV, DELIMITER ';', HEADER);


CREATE TABLE communes (
    cno SERIAL,
    nom_commune TEXT,
    departement CHAR(24),
    code_departemental TEXT,
    region TEXT,
    CONSTRAINT pk_communes PRIMARY KEY (cno)
);

INSERT INTO communes (nom_commune, departement, code_departemental, region)
SELECT DISTINCT ON (n9) n9, n6, n5, n7
FROM import;

CREATE TABLE etablissements (
    eno SERIAL,
    nom_etablissement TEXT,
    statut CHAR(33),
    academie TEXT,
    code_UAI CHAR(8),
    CONSTRAINT pk_etablissements PRIMARY KEY (eno)
);

INSERT INTO etablissements (nom_etablissement, statut, academie, code_UAI)
SELECT DISTINCT ON (n4) n4, n2, n8, n3
FROM import;

CREATE TABLE formations (
    fno SERIAL,
    nom_formation CHAR(290),
    selectivite TEXT,
    CONSTRAINT pk_formations PRIMARY KEY (fno)
);

INSERT INTO formations (nom_formation, selectivite)
SELECT DISTINCT ON (n10) n10, n11
FROM import;

CREATE TABLE candidats (
    cano SERIAL,
    c_total INTEGER,
    c_total_fem INTEGER,
    c_phas_prin INTEGER,
    c_phas_prin_gen INTEGER,
    c_phas_prin_gen_bours INTEGER,
    c_phas_prin_tech INTEGER,
    c_phas_prin_tech_bours INTEGER,
    c_phas_prin_pro INTEGER,
    c_phas_prin_pro_bours INTEGER,
    c_phas_prin_autr INTEGER,
    c_phas_comp INTEGER,
    c_phas_comp_gen INTEGER,
    c_phas_comp_gen_bours INTEGER,
    c_phas_comp_tech INTEGER,
    c_phas_comp_tech_bours INTEGER,
    c_phas_comp_pro INTEGER,
    c_phas_comp_pro_bours INTEGER,
    c_phas_comp_autr INTEGER,
    c_cod_aff_form INTEGER,
    CONSTRAINT pk_candidats PRIMARY KEY (cano)
);

INSERT INTO candidats (
    c_total,
    c_total_fem,
    c_phas_prin,
    c_phas_prin_gen,
    c_phas_prin_gen_bours,
    c_phas_prin_tech,
    c_phas_prin_tech_bours,
    c_phas_prin_pro,
    c_phas_prin_pro_bours,
    c_phas_prin_autr,
    c_phas_comp,
    c_phas_comp_gen,
    c_phas_comp_tech,
    c_phas_comp_pro,
    c_phas_comp_autr,
    c_cod_aff_form
)
SELECT n19, n20, n21, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n110
FROM import;

CREATE TABLE admis (
    cno INTEGER,
    eno INTEGER,
    fno INTEGER,
    cano INTEGER,
    a_total INTEGER,
    a_total_fem INTEGER,
    a_phas_princ INTEGER,
    a_phas_comp INTEGER,
    a_ouv_phase_princ INTEGER,
    a_avan_bac INTEGER,
    a_avan_fin_phas_princ FLOAT,
    a_bours INTEGER,
    a_gen INTEGER,
    a_tech INTEGER,
    a_pro INTEGER,
    a_autr INTEGER,
    a_gen_ment INTEGER,
    a_tech_ment INTEGER,
    a_pro_ment INTEGER,
    CONSTRAINT pk_admis PRIMARY KEY (cno, eno, fno, cano),
    CONSTRAINT fk_communes FOREIGN KEY (cno) REFERENCES communes (cno) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_etablissement FOREIGN KEY (eno) REFERENCES etablissements (eno) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_formations FOREIGN KEY (fno) REFERENCES formations (fno) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT fk_candidats FOREIGN KEY (cano) REFERENCES candidats (cano) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO admis (
    cno,
    eno,
    fno,
    cano,
    a_total,
    a_total_fem,
    a_phas_princ,
    a_phas_comp,
    a_ouv_phase_princ,
    a_avan_bac,
    a_avan_fin_phas_princ,
    a_bours,
    a_gen,
    a_tech,
    a_pro,
    a_autr,
    a_gen_ment,
    a_tech_ment,
    a_pro_ment
)
SELECT com.cno, e.eno, f.fno, cand.cano, n47, n48, n49, n50, n51, n52, n53, n55, n57, n58, n59, n60, n67, n68, n69
FROM import AS i 
    JOIN communes AS com ON com.nom_commune=i.n9
    JOIN etablissements AS e ON e.nom_etablissement=i.n4
    JOIN formations AS f ON f.nom_formation=i.n10
    JOIN candidats AS cand ON cand.c_cod_aff_form=i.n110;
